const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

MyLocation = process.argv[2]

if(!MyLocation){
    console.log('Please provide location')
}else{
    geocode(MyLocation, (error, {latitude, longlatitude,city}) => {
        if(error){
        return  console.log(error)
        }  
        forecast(latitude, longlatitude, (error, forecastData) => {
            if(error)
            {
                return console.log(error)
            }
            
            console.log(city)
            console.log(forecastData)

        })
        
    })
}



